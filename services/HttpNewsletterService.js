import axios from 'axios'

 const urlBase = 'https://twitter.us14.list-manage.com/subscribe'

export const newsletter = axios.create({
  baseURL: urlBase + '/post?',
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',    
  }
})

export default newsletter