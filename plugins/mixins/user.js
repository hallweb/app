import Vue from 'vue'
import { mapGetters } from 'vuex'

const User = {
    install(Vue, options) {
        Vue.mixin({
            computed: {
                ...mapGetters({
                    currentUser: 'auth/user',
                    loggedIn: 'auth/loggedIn'
                })
            }
        })
    }
}

Vue.use(User)